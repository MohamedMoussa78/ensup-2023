# Le code secret autorisé pour accéder à l'application
code_secret = "Padawan"

# Demander à l'utilisateur de saisir le code secret
saisie_utilisateur = input("Veuillez entrer le code secret : ")

# Vérifier si la saisie de l'utilisateur correspond au code secret autorisé
if saisie_utilisateur == code_secret:
    print("Accès autorisé. Vous pouvez utiliser l'application.")

    # Placez ici le code de l'application que l'utilisateur peut utiliser une fois l'accès autorisé.

else:
    print("Code secret incorrect. Accès refusé.")

# Demander à l'utilisateur de saisir la marque et le modèle de la voiture
marque_modele = input("Entrez la marque et le modèle de la voiture : ")

# Demander à l'utilisateur de saisir le prix HT de l'unité en euros
prix_ht_str = input("Entrez le prix HT de l'unité en euros : ")

# Convertir la saisie du prix HT en float
prix_ht = float(prix_ht_str)

# Demander à l'utilisateur si la voiture est électrique
est_electrique_str = input("La voiture est-elle électrique ? (Oui/Non) : ")
est_electrique = est_electrique_str.lower() == "oui"

# Taux de TVA en décimales
taux_tva = 0.05 if est_electrique else 0.20  # TVA à 5% pour les voitures électriques, sinon 20%

# Calcul du prix TTC
prix_ttc = prix_ht * (1 + taux_tva)

# Seuil pour déclencher la remise (20 000 €)
seuil_remise = 20000.00

# Remise en pourcentage (10%)
remise_pourcentage = 0.10

# Vérifier si le prix TTC est supérieur au seuil pour appliquer la remise
if prix_ttc > seuil_remise:
    remise = prix_ttc * remise_pourcentage
    prix_final = prix_ttc - remise
    print("Marque et modèle de la voiture :", marque_modele)
    print("Prix TTC avant remise :", prix_ttc, "euros")
    print("Remise de 10 % appliquée")
    print("Prix TTC après remise :", prix_final, "euros")
else:
    print("Marque et modèle de la voiture :", marque_modele)
    print("Prix TTC à payer (pas de remise) :", prix_ttc, "euros")