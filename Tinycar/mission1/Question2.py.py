# Objectif : Écrire le programme qui affiche le prix total TTC de la voiture en fonction du prix HT, du taux de tva (on prendra des valeurs au hasard).

prix_ht = 15000  # Prix hors taxe de la voiture en euros
taux_tva = 0.20    # Taux de TVA en décimales

# Calcul du prix TTC

prix_ttc = prix_ht * (1 + taux_tva)


# Résultat

print("Prix HT de la voiture :", prix_ht, "euros")
print("Taux de TVA :", taux_tva * 100, "%")
print("Prix TTC de la voiture :", prix_ttc, "euros")








