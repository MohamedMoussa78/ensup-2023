# Objectif : Compléter le programme de la question 2. Le programme doit commencer par initialiser le prix HT de l’unité en demandant la valeur à l’utilisateur. Ensuite, le programme affiche directement le prix TTC à payer. N’oubliez pas d’afficher un message explicite à l’utilisateur pour qu’il sache quoi saisir.


# Message pour demander à l'utilisateur de saisir le prix HT
prix_ht_str = input("15000")

# Convertir la saisie de l'utilisateur en un nombre à virgule flottante (float)
prix_ht = float(prix_ht_str)

# Taux de TVA en décimales 
taux_tva = 0.20

# Calcul du prix TTC
prix_ttc = prix_ht * (1 + taux_tva)

# Afficher le prix TTC
print("Le prix TTC à payer est de", prix_ttc, "euros.")
