# Objectif : Compléter le programme de la question 3. Le programme doit initialiser une nouvelle donnée : la marque et le modèle concernés en demandant les valeurs à l’utilisateur. Ensuite, le programme affiche directement le prix TTC à payer de ce produit. N’oubliez pas d’afficher un message explicite à l’utilisateur pour qu’il sache quoi saisir.

# Demander à l'utilisateur de saisir la marque et le modèle de la voiture
marque_modele = input("Mercedes")

# Demander à l'utilisateur de saisir le prix HT de l'unité
prix_ht_str = input("15000")

# Convertir la saisie de l'utilisateur en un nombre à virgule flottante pour le prix HT
prix_ht = (prix_ht_str)

# Taux de TVA en décimales
taux_tva = 0.20

# Calcul du prix TTC
prix_ttc = float (prix_ht) * (1 + taux_tva)

# Afficher le résultat avec la marque et le modèle
print("Mercedes", marque_modele)
print("Le prix TTC à payer est de", prix_ttc, "euros.")