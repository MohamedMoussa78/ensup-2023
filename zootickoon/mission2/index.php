< code >< !DOCTYPE html >
<html> _ _
  < tête >
    < méta charset= "utf-8" >
    <bec du zoo> Ma page de test < / bec du zoo >
  < /tête >
  < corps >
    < img src= "images/test.png" alt= "Mon image de test" >
  < /corps >
< /html >< /code >


<
<meta charset="utf-8">
    <title>Bec du Zoo - Bird Sanctuary</title>
  
 
</head>
  <body>
    <h1>Welcome to Bec du Zoo - Bird Sanctuary</h1>

    <section id="section1">
      <h2>Parrot Paradise</h2>
      <img src="images/parrot_paradise.jpg" alt="Parrot Paradise Image">
      <p>Code: BZ-001</p>
      <p>Description: Parrot Paradise is a vibrant home to a wide variety of colorful parrots.</p>
      <p>Area: 1200 square meters</p>
      <p>Number of Birds: 50</p>
      <p>Maximum Capacity: 100</p>

      </section>

    <section id="section2">
      <h2>Eagle's Roost</h2>
      <img src="images/eagle_roost.jpg" alt="Eagle's Roost Image">
      <p>Code: BZ-002</p>
      <p>Description: Eagle's Roost is a high-flying sanctuary for majestic eagles.</p>
      <p>Area: 800 square meters</p>
      <p>Number of Eagles: 10</p>
      <p>Maximum Capacity: 20</p>

      </section>

    <section id="section3">
      <h2>Flamingo Oasis</h2>
      <img src="images/flamingo_oasis.jpg" alt="Flamingo Oasis Image">
      <p>Code: BZ-003</p>
      <p>Description: Flamingo Oasis is a tranquil home to beautiful flamingos.</p>
      <p>Area: 1500 square meters</p>
      <p>Number of Flamingos: 30</p>
      <p>Maximum Capacity: 50</p>
   
     </section>

     
     <section id="section4">
      <h2>Pelican Bay</h2>
      <img src="images/pelican_bay.jpg" alt="Pelican Bay Image">
      <p>Code: BZ-004</p>
      <p>Description: Pelican Bay is a coastal haven for pelicans and other shorebirds.</p>
      <p>Area: 600 square meters</p>
      <p>Number of Pelicans: 15</p>
      <p>Maximum Capacity: 25</p>
    
    </section>

  

  </body
</body>
</html>

<style>
.zoom {
  padding: 45px;
  background-color: blue;
  transition: transform .3s; /* Animation */
  width: 200px;
  height: 200px;
  margin: 0 auto;
}

.zoom:hover {
  transform: scale(1.5); /* (190% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
}
</style>

<div class="zoom"></div>




