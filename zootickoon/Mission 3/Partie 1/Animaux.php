<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page d'accueil</title>
</head>
<body>

<?php
// Heure Temps réel
$heure = date("H");

// Afficher l'image en fonction de l'heure
if ($heure >= 6 && $heure < 12) {
   
    echo '<img src="zebre.jpg" alt="Zèbre le matin">';
} elseif ($heure >= 12 && $heure < 18) {

    echo '<img src="girafe.jpg" alt="Girafe l\'après-midi">';
} else {

    echo '<img src="panda.jpg" alt="Panda à midi">';
}
?>

</body>
</html>