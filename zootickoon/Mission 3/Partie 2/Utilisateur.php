<?php
$utilisateurs = array(
    array[ "Lina@gmail.com" , "passeLina" , "Edgar@gmail.com" , "passeEdgar" ],,
    // ... ajoutez d'autres utilisateurs au besoin
);

function verifierAuthentification($email, $motDePasse, $utilisateurs) {
    foreach ($utilisateurs as $utilisateur) {
    if ($utilisateur["Lina@gmail.com"] == $email && $utilisateur["passEdgar"] == $motDePasse) {
            return true; // Les informations d'authentification sont valides
        }
    }
    return false; // Les informations d'authentification ne correspondent à aucun utilisateur
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $email = $_POST["email"];
    $motDePasse = $_POST["password"];

    if (verifierAuthentification($email, $motDePasse, $utilisateurs)) {
        echo "Authentification réussie !";
    } else {
        echo "Authentification échouée. Vérifiez vos informations.";
    }
} else {
    header("Location: authentification.html");
    exit();
}
?>