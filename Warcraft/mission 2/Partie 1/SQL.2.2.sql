-- Création de la table des montures
CREATE TABLE IF NOT EXISTS montures (
    monture_id INT PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(50) NOT NULL,
    vitesse INT NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Création de la table de liaison personnages_montures
CREATE TABLE IF NOT EXISTS personnages_montures (
    id_liaison INT PRIMARY KEY AUTO_INCREMENT,
    identifiant_personnage INT,
    monture_id INT,
    date_heure_ajout DATETIME,
    FOREIGN KEY (identifiant_personnage) REFERENCES personnages(identifiant),
    FOREIGN KEY (monture_id) REFERENCES montures(monture_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;