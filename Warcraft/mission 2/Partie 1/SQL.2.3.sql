-- Attribuer une monture à Daril
INSERT INTO personnages_montures (identifiant_personnage, monture_id, date_heure_ajout)
VALUES (1, Daril, NOW(28/04/2024));

-- Attribuer une monture à Dunbar
INSERT INTO personnages_montures (identifiant_personnage, monture_id, date_heure_ajout)
VALUES (3, Dunbar, NOW(28/04/2024));

-- Attribuer une monture à Jaco
INSERT INTO personnages_montures (identifiant_personnage, monture_id, date_heure_ajout)
VALUES (2, Jaco, NOW(28/04/2024));