-- Attribuer plusieurs montures à Sperpinette
INSERT INTO personnages_montures (identifiant_personnage, monture_id, date_heure_ajout)
VALUES
(4, Monture numéro 1, NOW(28/04/2024)),
(4, Monture numéro 2, NOW(28/04/2024)),
(4, Monture numéro 3, NOW(28/04/2024));

-- Attribuer plusieurs montures à Saperlipopette
INSERT INTO personnages_montures (identifiant_personnage, monture_id, date_heure_ajout)
VALUES
(5, Monture numéro 4, NOW(28/04/2024)),
(5, Monture numéro 5, NOW(28/04/2024)),
(5, Monture numéro 6, NOW(28/04/2024));