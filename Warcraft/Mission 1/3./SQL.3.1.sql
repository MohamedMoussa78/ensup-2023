-- Table des personnages
CREATE TABLE IF NOT EXISTS personnages (
    identifiant INT PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(50) NOT NULL,
    date_inscription DATE NOT NULL,
    niveau INT NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Table des montures
CREATE TABLE IF NOT EXISTS montures (
    monture_id INT PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(50) NOT NULL,
    vitesse INT NOT NULL  -- Vitesse de la monture, par exemple
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Table de jointure pour relier les personnages aux montures
CREATE TABLE IF NOT EXISTS personnages_montures (
    identifiant INT,
    monture_id INT,
    PRIMARY KEY (identifiant, monture_id),
    FOREIGN KEY (identifiant) REFERENCES personnages(identifiant),
    FOREIGN KEY (monture_id) REFERENCES montures(monture_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;