-- Insérer la nouvelle classe "Prêtre" dans la table des classes
INSERT INTO classes (noms_classes, classes_power_type) VALUES ('Prêtre', 'mana');

-- Récupérer l'ID de la classe "Prêtre" pour l'utiliser dans les étapes suivantes
SET @prêtre_id = LAST_INSERT_ID();

-- Insérer des enregistrements dans la table de jointure classes_courses pour associer la classe "Prêtre" aux courses existantes
-- Supposons que les prêtres peuvent être associés à toutes les races pour cet exemple
INSERT INTO classes_courses (classes_id, courses_id)
SELECT @prêtre_id, courses_id FROM courses;

-- Insérer des enregistrements dans la table des personnages pour les nouveaux prêtres
-- Supposons que nous avons trois nouveaux prêtres de races différentes : Humain, Orc, et Elfe de la nuit
INSERT INTO personnages (nom, date_inscription, niveau) VALUES
('Nouveau Prêtre Humain', CURDATE(), 1),
('Nouveau Prêtre Orc', CURDATE(), 1),
('Nouveau Prêtre Elfe de la nuit', CURDATE(), 1);

-- Récupérer les IDs des nouveaux personnages pour les utiliser dans les étapes suivantes
SET @nouveau_prêtre_humain_id = LAST_INSERT_ID();
SET @nouveau_prêtre_orc_id = LAST_INSERT_ID() - 1;
SET @nouveau_prêtre_elfe_de_la_nuit_id = LAST_INSERT_ID() - 2;

-- Associer les nouveaux personnages à la classe "Prêtre" dans la table de jointure personnages_classes
INSERT INTO personnages_classes (identifiant, classes_id)
VALUES
(@nouveau_prêtre_humain_id, @prêtre_id),
(@nouveau_prêtre_orc_id, @prêtre_id),
(@nouveau_prêtre_elfe_de_la_nuit_id, @prêtre_id);

-- Associer les nouveaux personnages à leurs races respectives dans la table de jointure personnages_courses
-- Supposons que les courses pour les races Humain, Orc, et Elfe de la nuit ont les IDs respectifs 1, 2, et 4
INSERT INTO personnages_courses (identifiant, courses_id)
VALUES
(@nouveau_prêtre_humain_id, 1),
(@nouveau_prêtre_orc_id, 2),
(@nouveau_prêtre_elfe_de_la_nuit_id, 4);