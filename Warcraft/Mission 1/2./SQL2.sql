-- Table des classes
CREATE TABLE IF NOT EXISTS classes (
    classes_id INT PRIMARY KEY AUTO_INCREMENT,
    noms_classes VARCHAR(20) NOT NULL,
    classes_power_type VARCHAR(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Table des courses
CREATE TABLE IF NOT EXISTS courses (
    courses_id INT PRIMARY KEY AUTO_INCREMENT,
    courses_name VARCHAR(20) NOT NULL,
    courses_side VARCHAR(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Table de jointure pour relier les classes aux courses
CREATE TABLE IF NOT EXISTS classes_courses (
    classes_id INT,
    courses_id INT,
    PRIMARY KEY (classes_id, courses_id),
    FOREIGN KEY (classes_id) REFERENCES classes(classes_id),
    FOREIGN KEY (courses_id) REFERENCES courses(courses_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Table des personnages
CREATE TABLE IF NOT EXISTS personnages (
    identifiant INT PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(50) NOT NULL,
    date_inscription DATE NOT NULL,
    niveau INT NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Table de jointure pour relier les personnages aux classes
CREATE TABLE IF NOT EXISTS personnages_classes (
    identifiant INT,
    classes_id INT,
    PRIMARY KEY (identifiant, classes_id),
    FOREIGN KEY (identifiant) REFERENCES personnages(identifiant),
    FOREIGN KEY (classes_id) REFERENCES classes(classes_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Table de jointure pour relier les personnages aux courses
CREATE TABLE IF NOT EXISTS personnages_courses (
    identifiant INT,
    courses_id INT,
    PRIMARY KEY (identifiant, courses_id),
    FOREIGN KEY (identifiant) REFERENCES personnages(identifiant),
    FOREIGN KEY (courses_id) REFERENCES courses(courses_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;