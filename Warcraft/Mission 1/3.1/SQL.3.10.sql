-- Modification de la table de jointure personnages_montures pour inclure une clé étrangère vers la table des montures
ALTER TABLE personnages_montures
ADD COLUMN monture_id INT,
ADD FOREIGN KEY (monture_id) REFERENCES montures(monture_id);