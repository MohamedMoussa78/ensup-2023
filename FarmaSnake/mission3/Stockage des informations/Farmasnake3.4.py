def decimal_to_binary(decimal_ip):
    # Séparer les octets de l'adresse IP
    octets = decimal_ip.split('.')
    
    # Convertir chaque octet en binaire et le stocker dans une liste
    binary_octets = [bin(int(octet))[2:].zfill(8) for octet in octets]
    
    return binary_octets

def main():
    # Adresses IP en décimal
    ip_addresses_decimal = ['192.168.1.1', '10.0.0.1', '172.16.0.1', '192.0.2.1', '198.51.100.1', '203.0.113.1']

    # Convertir en binaire et stocker dans une liste
    ip_addresses_binary = [decimal_to_binary(ip) for ip in ip_addresses_decimal]

    print("Affichage des adresses IP :")
    for i in range(len(ip_addresses_decimal)):
        print("Adresse IP en décimal :", ip_addresses_decimal[i])
        print("Adresse IP en binaire :", '.'.join(ip_addresses_binary[i]))
        print()  # Ajouter une ligne vide entre chaque adresse IP

if __name__ == "__main__":
    main()