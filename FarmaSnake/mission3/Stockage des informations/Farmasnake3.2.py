def ipv6_to_list(ipv6_address):
    # Supprimer les crochets s'ils sont présents
    ipv6_address = ipv6_address.replace('[', '').replace(']', '')

    # Séparer les sections de l'adresse IP IPv6
    sections = ipv6_address.split(':')

    # Stocker l'adresse IP IPv6 sous forme de liste
    ipv6_list = sections

    return ipv6_list

def main():
    # Adresse IP IPv6
    ipv6_address = '2001:0db8:85a3:0000:0000:8a2e:0370:7334'

    # Stocker l'adresse IP IPv6 sous forme de liste
    ipv6_list = ipv6_to_list(ipv6_address)

    print("Adresse IP IPv6:", ipv6_address)
    print("Adresse IP IPv6 sous forme de liste:", ipv6_list)

if __name__ == "__main__":
    main()