def binary_to_decimal(binary_string):
    # Convertir la chaîne binaire en nombre décimal
    decimal_number = int(binary_string, 2)
    return decimal_number

# Chaîne binaire de 8 bits
binary_string = '11010101'

# Convertir en nombre décimal
decimal_number = binary_to_decimal(binary_string)

print("Chaîne binaire:", binary_string)
print("Nombre décimal correspondant:", decimal_number)