# Importer les fonctions de MaBibliotheque.py
from MaBibliotheque import dec2bin, bin2dec

def main():
    print("Choisissez le type de conversion :")
    print("1. Décimal vers binaire")
    print("2. Binaire vers décimal")

    choice = input("Entrez votre choix (1 ou 2) : ")

    if choice == '1':
        decimal_input = int(input("Entrez un nombre décimal : "))
        binary_output = dec2bin(decimal_input)
        print("Le nombre binaire correspondant est :", binary_output)
    elif choice == '2':
        binary_input = input("Entrez un nombre binaire (8 bits) : ")
        decimal_output = bin2dec(binary_input)
        print("Le nombre décimal correspondant est :", decimal_output)
    else:
        print("Choix invalide. Veuillez entrer 1 ou 2.")

if __name__ == "__main__":
    main()