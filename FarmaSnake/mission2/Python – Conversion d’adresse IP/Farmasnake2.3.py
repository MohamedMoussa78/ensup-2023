def dec2bin(decimal_number):
    # Convertir le nombre décimal en binaire
    binary_string = bin(decimal_number)[2:]
    return binary_string

# Exemple d'utilisation de la fonction
decimal_number = 13
binary_number = dec2bin(decimal_number)

print("Nombre décimal:", decimal_number)
print("Nombre binaire correspondant:", binary_number)