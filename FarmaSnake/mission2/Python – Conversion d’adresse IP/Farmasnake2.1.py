def decimal_to_binary(ip_address):
    # Séparer les octets de l'adresse IP
    octets = ip_address.split('.')
    
    # Convertir chaque octet en binaire et le stocker dans une liste
    binary_octets = [bin(int(octet))[2:].zfill(8) for octet in octets]
    
    # Joindre les octets binaires pour former l'adresse IP binaire complète
    binary_ip = '.'.join(binary_octets)
    
    return binary_ip

# Adresse IP en décimal
ip_address_decimal = '192.168.1.1'

# Convertir en binaire
ip_address_binary = decimal_to_binary(ip_address_decimal)

print("Adresse IP en décimal:", ip_address_decimal)
print("Adresse IP en binaire:", ip_address_binary)