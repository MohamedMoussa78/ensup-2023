def bin2dec(binary_string):
    # Convertir le nombre binaire en décimal
    decimal_number = int(binary_string, 2)
    return decimal_number

# Exemple d'utilisation de la fonction
binary_string = '11010101'
decimal_number = bin2dec(binary_string)

print("Nombre binaire:", binary_string)
print("Nombre décimal correspondant:", decimal_number)
