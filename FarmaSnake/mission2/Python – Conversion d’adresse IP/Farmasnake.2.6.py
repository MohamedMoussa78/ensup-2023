# MaBibliotheque.py

def dec2bin(decimal_number):
    # Convertir le nombre décimal en binaire
    binary_string = bin(decimal_number)[2:]
    return binary_string

def bin2dec(binary_string):
    # Convertir le nombre binaire en décimal
    decimal_number = int(binary_string, 2)
    return decimal_number

