# Demande à l'utilisateur un nombre
nombre = input("Veuillez entrer un nombre : ")

# Affiche le nombre saisi par l'utilisateur
print("Le nombre que vous avez saisi est :", nombre)

# Vérifie si le nombre est dans la plage demandée
if 0 <= nombre <= 255:
    print("Correct")
else:
    print("Incorrect")